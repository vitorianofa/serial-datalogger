import csv
import time

def save_to_file(filename, line):
    with open(filename,"a") as f:
        writer = csv.writer(f,delimiter=",")
        writer.writerow([time.strftime('%c'), line])
