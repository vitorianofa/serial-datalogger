from ListPorts import serial_ports
from SaveToFile import save_to_file
import serial

if __name__ == '__main__':
    print('Available ports:')
    ports = serial_ports()
    for i in range(0,len(ports)):
        print(" %d - %s" % (i, ports[i]))

    opt = int(input("Choose an option: "))
    port = ports[opt]

    ser = serial.Serial(port, 115200, timeout=1)
    ser.flush()
    while 1:
        try:
            line = ser.readline().decode("utf-8")
            line = line.replace('\n','')
            if line:
                print(line)
                save_to_file("teste.csv",line)
        except ser.SerialTimeoutException:
            print('Data could not be read')
