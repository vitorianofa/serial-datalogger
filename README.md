Data Logger from serial port (usefull for Arduino, ESP, etc)

Save data coming from serial port to a file.

Run 
`DataLogger.py`

Choose a port (COMX for windows, dev/*** for ubuntu, etc)

The log will be saved on a csv file in the working directory.